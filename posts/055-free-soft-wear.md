title: 55: Free Soft Wear
date: 2023-01-31 22:05
tags: crafts, free soft wear
slug: 055-free-soft-wear
summary: Morgan talks about "Free Soft Wear": textile processes under free culture licenses!
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/055-free-soft-wear.ogg" length:"31190261" duration:"00:37:55"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/055-free-soft-wear.mp3" length:"36404185" duration:"00:37:55"
---
Morgan talks about "Free Soft Wear": textile processes under free culture licenses!

**Links:**

 - [Morgan's talk about Free Soft Wear at the Creative Freedom Summit](https://peertube.linuxrocks.online/w/sp8gqwcxHAWQFuM2jcqkKn)

 - [Elena of Valhalla](https://www.trueelena.org/)’s [repository of CC BY-SA sewing patterns](https://sewing-patterns.trueelena.org/)

 - [Morgan's blog](https://mlemmer.org)

   - [Free Soft Wear index](https://mlemmer.org/free_soft_wear_index/)

   - [Dice bag and simple skirt tutorials](https://mlemmer.org/blog/baskic_sewing_tutorial/)

   - [RSI Glove pattern](https://mlemmer.org/blog/RSIgloves/)

   - [Simple sweater](https://mlemmer.org/blog/simple_sweater/)

   - [Layered Skirt](https://mlemmer.org/blog/layered_skirt_tutorial/)

 - [Kat Walsh](https://creativecommons.org/author/katwalshcreativecommons-org/) or [@kat@stareinto.space](https://stareinto.space/@kat)

 - [Tall Dog Electronics face mask](https://talldog.dozuki.com/Guide/How+to+make+a+respirator-style+face+mask/2) (You may recognize Dan and Tall Dog Electronics of [TinyNES fame](https://fossandcrafts.org/episodes/39-tinynes.html))

 - [Wikimedia Commons](https://commons.wikimedia.org)

 - [Project Gutenberg](https://www.gutenberg.org/)

 - [Learning the sewing machine](https://fossandcrafts.org/episodes/42-learning-the-sewing-machine.html)

 - [RSI episode](https://fossandcrafts.org/episodes/43-repetitive-strain-injuries.html)

 - [FreeSewing](https://freesewing.org/) (an open source software project that creates made-to-measure creative commons licensed sewing patterns)

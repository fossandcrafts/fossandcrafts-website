title: 5: Milkytracker, chiptunes, and that intro music
date: 2020-08-13 17:40
tags: episode, foss, crafts, milkytracker, music
slug: 5-milkytracker
summary: The making of the intro song is used as an avenue to explore Milkytracker and a bit of sound theory
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/005-milkytracker.ogg" length:"27889918" duration:"00:39:45"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/005-milkytracker.mp3" length:"38186018" duration:"00:39:45"
---
Chris's journey of making the intro music is used as a backdrop to
explore how to make music in [Milkytracker](https://milkytracker.org/),
a FOSS program for making tracker music, as well as to explore a bit
of sound theory, what chiptunes and tracker music are, and even a bit
of exploring what it's like to learn something new even when you
aren't necessarily very good yet.

**Links:**

 - [Milkytracker](https://milkytracker.org/)

 - [sfxr](http://www.drpetter.se/project_sfxr.html)

 - [drpetter's sound theory and synthesis page](http://www.drpetter.se/article_sound.html)

 - [musagi](http://www.drpetter.se/project_musagi.html) and the
   [musagi tutorial](http://www.drpetter.se/tutorial_musagi1.html)

 - [The Impulse Project](http://impulseproject.info/)

 - The [Commodore 64](https://en.wikipedia.org/wiki/Commodore_64)
   computer and its famous [SID chip](https://en.wikipedia.org/wiki/MOS_Technology_6581)

 - [c64.com](http://www.c64.com/), an archive of Commodore 64 games/programs
   (pretty much all proprietary though).
   Many of these have interesting cracked demos that are as interesting as
   the programs themselves.

 - [Monty on the Run](http://www.c64.com/games/570)
   with its music by the famous Commodore 64 composer,
   [Rob Hubbard](https://en.wikipedia.org/wiki/Rob_Hubbard)

   - Listen to the [Monty on the Run main theme](https://www.youtube.com/watch?v=4EcgruWlXnQ)

   - [Rob Hubbard's Music: Disassembled, Commented and Explained](http://sid.kubarth.com/articles/rob_hubbards_music.txt)

   - Not shown in the podcast but you really also ought to listen to the
     [Commando theme for the Commodore 64](https://www.youtube.com/watch?v=qrQuR1LHAVI)

 - [Moments by Mr. Lou (mp3)](https://milkytracker.org/songs/Mr.Lou-Moments.mp3)
   and the original [XM file (zipped)](https://milkytracker.org/songs/Mr.Lou-Moments.zip)
   Really worth listening to the XM in Milkytracker so you can see
   how things work.

 - More cool music on the bottom of
   [Milkytracker's downloads page](https://milkytracker.org/downloads/)
   and especially on [The Mod Archive](http://modarchive.org/).

 - The [demoscene](https://en.wikipedia.org/wiki/Demoscene)

 - [Chiptunes](https://en.wikipedia.org/wiki/Chiptune)

 - [Music trackers](https://en.wikipedia.org/wiki/Music_tracker)

 - [Famitracker](http://www.famitracker.com/)
   (Free software so why the heck is it Windows-only still?
   Someone [finish porting it](https://github.com/Prichman/famitracker-qt)!)

 - [Milkytracker's documentation page](https://milkytracker.org/documentation/)
   has of course [its own manual](https://milkytracker.org/docs/MilkyTracker.html)
   but also a number of interesting historical music tracking guides

 - [Brandon Walsh's milkytracker / chiptune tutorials](https://www.youtube.com/watch?v=N2s04YYO0Wg&list=PLgQLAgklMBxEuPzQUNKc2xSJu5pXx7xVx)
   (Content warning in that he does say an ablist slur somewhere in those videos.)

 - Music theory stuff
 
   - [Open Music Theory](http://openmusictheory.com/)
   - [8-bit Music Theory](https://www.youtube.com/channel/UCeZLO2VgbZHeDcongKzzfOw)
   - [Learn music theory in half an hour](https://www.youtube.com/watch?v=rgaTLrZGlk0)
     (well, some of it)

 - [freesound](https://freesound.org/), amazing commons of useful samples
   for your music composition needs

 - I guess maybe you want to look at Chris's sound file sources (but probably not)
   (All CC BY-SA 3.0, like the show)

   - [Conversations with a Computer](https://dustycloud.org/misc/conversations-with-a-computer.xm)

   - [Dollhouse](https://dustycloud.org/misc/dollhouse.xm)

   - [Ecto House](https://dustycloud.org/misc/ecto-house.xm)

   - [the arpeggio example shown in the show](https://dustycloud.org/misc/siddy-start.xm)

   - And yes, the [FOSS and Crafts intro theme](https://dustycloud.org/misc/foss-and-crafts-intro.xm)

Made it all the way to the end of the podcast *and* this blogpost?
I guess you really did [stay awhile...](https://www.youtube.com/watch?v=i1_fDwX1VVY)

title: 62: Blender
date: 2024-03-03 14:40
tags: blender, crafts, foss
slug: 062-blender
summary: An overview of Blender, the FOSS graphics 3d and (increasingly 2d) powerhouse!
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/062-blender.ogg" length:"42383193" duration:"00:47:11"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/062-blender.mp3" length:"45301694" duration:"00:47:11"
---
Blender, the absolute powerhouse of FOSS 3d (and increasingly 2d) graphics!
We give an overview of the software's history, some personal history of
our relationships to the software, what it can do, and where we're excited
to see it go!

**Links:**

- [Blender](https://blender.org/)
- [Blender history](https://www.blender.org/about/history/)
- [Grease pencil](https://docs.blender.org/manual/en/latest/grease_pencil/index.html)
- Some historical Blender videos from the NeoGeo and Not a Number days: [Did It, Done It](https://www.youtube.com/watch?v=WKAQNBaZ_I8), [Not a Number commercial](https://www.youtube.com/watch?v=2AMubc7C1vw), [Come and See](https://www.youtube.com/watch?v=2qyVRFI0B6g)
- [Elephants Dream](https://orange.blender.org/), aka Project Orange
- [Big Buck Bunny](https://peach.blender.org/)
- Previous episodes on blender:
  - [Blender for open movie productions and education](https://fossandcrafts.org/episodes/16-bassam-kurdali-blender-open-movies-education.html)
  - [Sophie Jantak on pet portraits and Blender's Grease Pencil](https://fossandcrafts.org/episodes/48-sophie-jantak-pet-portraits-grease-pencil.html)
- Blender Conference videos mentioned:
  - [Inklines Across The Spiderverse](https://conference.blender.org/2023/presentations/1928/)
  - [My Journey Across the Spider-Verse: from Hobbyist to Hollywood](https://conference.blender.org/2023/presentations/1823/)
  - [Forensic Architecture - spatial analysis for human rights cases](https://www.youtube.com/watch?v=RWPdSJ1-Dvs)
- [The MediaGoblin campaign video](https://www.youtube.com/watch?v=xsi_VyzbDrE) (well, the second one)
- [14th anniversary animation gift to Morgan](https://www.youtube.com/watch?v=MUIMYnSbfPQ)
- [In Unexpected Places](https://www.youtube.com/watch?v=88JUfWLJJ5g)
- [Seams to Sewing Pattern](http://thomaskole.nl/s2s/) (a Blender plugin for making clothes and stuffed animals!) (could we make [Free Soft Wear](https://fossandcrafts.org/episodes/055-free-soft-wear.html) patterns with it?)
- [Wing It!](https://studio.blender.org/films/wing-it/)
- [Wing It! Production Logs](https://www.youtube.com/watch?v=p6ESaby2KDA&list=PLav47HAVZMjkgw-ueySUvvq1aynJ4s7ry) and [Blenderheads](https://www.youtube.com/watch?v=Kc5XEcHGIHU&list=PLa1F2ddGya_8I8QCMCKlQUOQpgOga2nZ-)
- Episodes about lisp, because obviously Blender needs more lisp (who's going to do it):
  - [What is Lisp?](https://fossandcrafts.org/episodes/47-what-is-lisp.html)
  - [Lisp but Beautiful, Lisp for Everyone](https://fossandcrafts.org/episodes/49-lisp-but-beautiful-lisp-for-everyone.html)
  

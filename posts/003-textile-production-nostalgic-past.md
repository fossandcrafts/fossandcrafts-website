title: 3: Textile production and a nostalgic past
date: 2020-07-30 09:45
tags: episode
slug: 3-textile-production-and-a-nostalgic-past
summary: A walkthrough of the steps to produce textiles from raw materials, plus how textile production has been used in political discourse historically
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/003-nostalgic-textile-production.ogg" length:"26258531" duration:"00:38:55"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/003-nostalgic-textile-production.mp3" length:"37362169" duration:"00:38:55"
---
These days textile production is mostly automated aside from some
niche markets and craft production.  Craft production of textiles
today taps into a vision of a nostalgic past, often evoking memories
of a time the audience member wasn't there for.  It turns out this
potent imagery has been used not just for inspiring hobbyist crafters
everywhere to pull out the drop spindle and knitting needles, but also
by political participants going back all the way to (at least) Ancient
Rome to try to steer a particular narrative.  Follow some of that
history from past to present, and hear from Morgan about how the whole
process of textile production works starting from raw
materials... from sheep to sweater!

**Links and references:**

 - Suetonius, [Life of Augustus](
http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Suetonius/12Caesars/Augustus*.html);
   scroll down to section 73 for the secton on his humble furnishings and
   home-made clothing.

 - Laurel Thatcher Ulrich.
   *The Age of Homespun: Objects and Stories in the Creation of an American Myth.* New York: Vintage Books, 2001.
   Or a shorter
   [article](https://www.amrevmuseum.org/read-the-revolution/history/age-homespun)
   sourced from this book.

 - Trivedi, Lisa. *Clothing Gandhi's Nation : Homespun and Modern India.*
   Bloomington :Indiana University Press, 2007. Or a shorter
   [article](http://textileartscenter.com/blog/thread-and-roses-gandhis-homespun-revolution/)
   sourced from this book.

 - Obniski, Monica. [“The Arts and Crafts Movement in America.”](http://www.metmuseum.org/toah/hd/acam/hd_acam.htm)
   In *Heilbrunn Timeline of Art History.* New York: The Metropolitan
   Museum of Art, (June 2008)

 - [Washing Fleece](https://www.youtube.com/playlist?list=PLfF6fnck_2Bv-nGHERvbYp3mKaG9aM8TZ)

 - [Carding Fiber](https://en.wikipedia.org/wiki/Carding)

 - [Combing Fiber](https://en.wikipedia.org/wiki/Combing)

 - [Spinning](https://en.wikipedia.org/wiki/Spinning_(textiles)), [Spinning with a drop spindle and distaff](https://youtu.be/pFSdZdHCh40)

 - [Yarn Bombing](https://en.wikipedia.org/wiki/Yarn_bombing)

 - Pussy Hat Project's [official website](https://www.pussyhatproject.com/).
   The [Wikipedia](https://en.wikipedia.org/wiki/Pussyhat#Racism_and_transphobia_charges)
   article addresses the raised issues of transphobia and racism and
   links to further sources.

 - [Tiny Pricks Project](https://www.tinypricksproject.com/)

 - The [mask pattern](https://talldog.dozuki.com/Guide/How+to+make+a+respirator-style+face+mask/2?fbclid=IwAR3oC1t6n17WrD-QzMA8LI9j57zupiUB3VWftmWDfChrEtOZ5oypFBoyID0)
   that Morgan uses, made by our friend [Dan Gilbert](https://www.tall-dog.com/),
   who makes cool open things, check them out.

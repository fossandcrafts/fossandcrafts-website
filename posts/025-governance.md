title: 25: Governance, Leadership, and Founder's Syndrome
date: 2021-03-28 19:05
tags: governance
slug: 25-governance-leadership-founders-syndrome
summary: A discussion of RMS's surprise re-appointment by the FSF board to the FSF board within a general framework of governance concpts
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/025-governance.ogg" length:"78889507" duration:"01:35:36"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/025-governance.mp3" length:"91777958" duration:"01:35:36"
---
What story does an institution tell about itself?
To whom does a governance structure and its leadership serve?
To what degree are leaders within a governing institution subject
or exempt to the rules of the governed?
We use this framework to discuss the unexpected announcement of Richard
Stallman's re-appointment to the FSF board, by the FSF board.

Content warning: depression and sexual harassment are both mentioned
in this episode.

**Links:**

 - [Founder's Syndrome](https://en.wikipedia.org/wiki/Founder%27s_syndrome)
   (Wikipedia article)

 - [Statement on the Re-election of Richard Stallman to the FSF Board](https://www.eff.org/deeplinks/2021/03/statement-re-election-richard-stallman-fsf-board) by the [EFF](https://www.eff.org/)

 - [An open letter to remove Richard M. Stallman from all leadership positions](https://rms-open-letter.github.io/)

 - [Microblog post expressing lack of foreknowledge of announcement by FSF staff and Libreplanet volunteers](https://twitter.com/fsf/status/1374399897558917128)

 - [Not The First Time We Tried (FSF, GNU, RMS, etc.)](https://www.harihareswara.net/ces.shtml) by [Sumana Harihareswara](https://www.harihareswara.net/)

 - Kat Walsh's statement of [making arguments in opposition to RMS's reinstatement to the board](https://mastodon.social/@mindspillage/105940851460229358) ([Twitter version](https://twitter.com/mindspillage/status/1374448587388588037)) and [announcement of resignation](https://mastodon.social/@mindspillage/105948765536809476) ([Twitter version](https://twitter.com/mindspillage/status/1374955150054289412))

 - [The Free Software Foundation and Richard Stallman](https://mako.cc/copyrighteous/the-free-software-foundation-and-richard-stallman) by [Benjamin Mako Hill](https://mako.cc/), longtime FOSS advocate, former member of FSF board 

 - [Programming is Forgetting: Toward a New Hacker Ethic](http://opentranscripts.org/transcript/programming-forgetting-new-hacker-ethic/)

 - [Documentation of accusations of women being harassed by RMS](https://selamjie.medium.com/remove-richard-stallman-appendix-a-a7e41e784f88)

 - [Political cartoon between RMS and Chris](https://stallman.org/images/trade-treaty-comic.jpg) from when Chris was in college

 - [Re: Bug in emacs tetris](https://lists.gnu.org/archive/html/emacs-devel/2007-02/msg01316.html)

 - [GNU Emacs and XEmacs](https://www.emacswiki.org/emacs/EmacsAndXEmacs)

 - [2018 Letter to the board in response to RMS's behavior and the board's lack of response](https://announce.asheesh.org/2018/12/libreplanet-speakers-ask-fsf-board-about-safe-space-policy-get-no-real-answer/)

 - [Marianne Corvellec’s Libre Planet 2017 talk “The GNU Philosophy: Ethics beyond ethics”](https://media.libreplanet.org/u/libreplanet/m/the-gnu-philosophy-ethics-beyond-ethics/) where RMS says "I'm the president of the Free Software Foundation... so I don’t have to follow the rules"

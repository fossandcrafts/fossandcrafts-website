title: 17: Gardening, from seedling to seasoned
date: 2020-11-28 09:50
tags: episode, guest, gardening, crafts
slug: 17-gardening-seedling-to-seasoned
summary: We're joined by our friend Tristan to talk about gardening experiences, from newbies (us) to the wise (Tristan and others who are not us)
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/017-gardening-seedling-to-seasoned.ogg" length:"54412120" duration:"01:26:19"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/017-gardening-seedling-to-seasoned.mp3" length:"82880433" duration:"01:26:19"
---
We're joined by our friend Tristan to talk about gardening
experiences, from newbies (us) to the wise (Tristan and others who are
not us).
We (Morgan and Chris) have just started seriously gardening this year,
and have learned a lot about what works and what doesn't.
And it turns out that people who have been doing it for years (such as
Tristan) still have a lot of successes but also a lot of failures.
But those can be fun too!

![Duck lounging](/static/images/blog/duck-lounging.jpg)

![Cold frame sketch](/static/images/blog/cold-frame-sketch.jpg)

![Cold frame closed](/static/images/blog/cold-frame-closed.jpg)

![Cold frame open](/static/images/blog/cold-frame-open.jpg)

![Cold frame inside](/static/images/blog/cold-frame-inside.jpg)

**Links:**

 - [Stocking Up](https://www.simonandschuster.com/books/Stocking-Up/Carol-Hupping/9780671693954)
   (book on preserving)
 - [No dig sheet mulching](https://www.youtube.com/watch?v=0LH6-w57Slw)

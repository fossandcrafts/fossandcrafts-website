title: 56: Make your own dehydrated meals
date: 2023-02-28 22:00
tags: crafts, food
slug: 056-dehydrated-meals
summary: How to make your own dehydrated meals for fun and travel
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/056-dehydrated-meals.ogg" length:"34965008" duration:"00:41:33"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/056-dehydrated-meals.mp3" length:"39895415" duration:"00:41:33"
---
In yet another deep dive into yet another weird hobby of Christine's,
we talk about how to make your own dehydrated meals!
Why the heck would you want to do this?
Well, maybe you want more consistent or dietary needs friendly travel
food!
Maybe you want to go camping or hiking!
Maybe you're sick of deciding what's for lunch and you just want to
scoop a cup of meal out of a jar on your desk every day!
Maybe you want to weird out your fellow conference-goers as you turn a
dry powder into a fully cooked meal with hot water and hot water
alone!

**Links:**

- Making dehydrated meals overview (Christine's Kitchen 0):
  [[YouTube](https://www.youtube.com/watch?v=KaY8kryWBT4)]
  [[PeerTube](https://share.tube/w/2Cbxaw1kmzz1SXgkgTZesm)]

- [Backpacking chef](https://www.backpackingchef.com/)

- [Dishwasher cooking](https://www.npr.org/sections/thesalt/2013/08/25/214799882/dishwasher-cooking-make-your-dinner-while-cleaning-the-plates)
  (yes it is a thing)

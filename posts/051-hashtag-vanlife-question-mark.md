title: 51: #vanlife...?
date: 2022-10-01 15:00
tags: crafts, vanlife
slug: 051-hashtag-vanlife-question-mark
summary: Morgan and Christine walk through their (well, Morgan's) renovation of a cargo van into a campervan.
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/051-hashtag-vanlife-question-mark.ogg" length:"41285354" duration:"00:48:14"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/051-hashtag-vanlife-question-mark.mp3" length:"46317356" duration:"00:48:14"
---
[![A peek into the van](/static/images/blog/vanlife6.jpg)](/static/images/blog/vanlife6.jpg)

Morgan and Christine walk through their (well, Morgan's) renovation of
a cargo van into a campervan.  This is a very crafty episode, but we do
work in a few analogies to some FOSS (and open hardware) things!

Show notes at the end, but how about a quick visual van tour?

Back of the van, wide open!

[![Van from back, doors wide open](/static/images/blog/vanlife4.jpg)](/static/images/blog/vanlife4.jpg)

A closer look...

[![Van from back, closer](/static/images/blog/vanlife5.jpg)](/static/images/blog/vanlife5.jpg)

Actually, let's move that solar panel aside...

[![Van from back, move that solar panel aside](/static/images/blog/vanlife6.jpg)](/static/images/blog/vanlife6.jpg)

Here's a better view of the cabinet with all the equipment attached:

[![View of cabinet with cargo net, dowels, cargo strap on](/static/images/blog/vanlife7.jpg)](/static/images/blog/vanlife7.jpg)

Here's what the van looks like if you come in the side door:

[![View from side entrance, straight view](/static/images/blog/vanlife1.jpg)](/static/images/blog/vanlife1.jpg)

Another, more diagonal view:

[![View from side entrance, diagonal view](/static/images/blog/vanlife2.jpg)](/static/images/blog/vanlife2.jpg)

Safety first!

[![Looking at cabinet with safety equipment from side](/static/images/blog/vanlife3.jpg)](/static/images/blog/vanlife3.jpg)

Window covers, custom fit!  Reflectix goes out, fabric goes in.

[![Window cover](/static/images/blog/vanlife8.jpg)](/static/images/blog/vanlife8.jpg)

The cabinet with the cargo net off...

[![View of cabinet with cargo net off](/static/images/blog/vanlife9.jpg)](/static/images/blog/vanlife9.jpg)

And one more view!

[![View of cabinet with cargo net off, diagonally](/static/images/blog/vanlife10.jpg)](/static/images/blog/vanlife10.jpg)

**Links:**

 - [Cheap RV Living channel on YouTube](https://www.youtube.com/channel/UCAj7O3LCDbkIR54hAn6Zz7A)
 - [Vanlife subreddit](https://www.reddit.com/r/VanLife/)
 - [Built to Go! A #Vanlife Podcast](https://builttogo.podbean.com/)
 - [Foresty Forest](https://www.youtube.com/user/forestyforest)

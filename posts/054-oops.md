title: 54: Oops!
date: 2022-12-27 19:20
tags: foss, crafts
slug: 054-oops
summary: Everyone goofs sometimes.  Today we talk accidents... some happy, some not!
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/054-oops.ogg" length:"30229638" duration:"00:35:12"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/054-oops.mp3" length:"33800291" duration:"00:35:12"
---
Everyone goofs sometimes.  Today we talk accidents... some happy, some not!

**Links:**

 - [Decaf coffee](https://en.wikipedia.org/wiki/Decaffeination) and [history of penicillin](https://en.wikipedia.org/wiki/History_of_penicillin), your pop-sci "accidents of history" stories of the day.  Look, this is admittedly kind of a fluff episode.
 - Have we linked to [Worse is Better](https://www.dreamsongs.com/WorseIsBetter.html) before?  We did?  In the [lisp episode](/episodes/47-what-is-lisp.html)?
 - And here's the [Terminal Phase episode](/episodes/052-terminal-phase.html)

title: 37: Salt on Resilience in FOSS
date: 2021-11-02 21:00
tags: foss, interview, communities
slug: 37-salt-on-resilience-in-foss
summary: Salt joins us to talk about his dissertation on FOSS community resilience and founder decisions
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/037-salt-on-resilience-in-foss.ogg" length:"35313730" duration:"00:58:52"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/037-salt-on-resilience-in-foss.mp3" length:"56520547" duration:"00:58:52"
---
[Wm Salt Hale](https://sal.td/) joins us to talk about his
dissertation on resilience in FOSS communities (especially after
crisis events), the kind of impacts founder decisions can have on
long-term community development, especially as seen through reactions
to software vulnerabilities and license decisions.

Also!  Salt mentions that we're
[keynoting](https://osem.seagl.org/conferences/seagl2021/program/proposals/866)
at [SeaGL](https://seagl.org/) this weekend!  It's an online
conference, so maybe we'll see you there!

**Links:**

 - [Wm Salt Hale](https://sal.td/)

 - [Salt's master's thesis: Resilience in Free/Libre/Open Source Software](https://www.altsalt.net/publications/Resilience-in-FLOSS/)

 - [Community Data Science Collective](https://wiki.communitydata.science/Main_Page)
 
 - [Benjamin Mako Hill](https://mako.cc/)
 
 - [Wikipedia article on Heartbleed](https://en.wikipedia.org/wiki/Heartbleed)
 
 - [Debian](https://www.debian.org/)
 
 - [Champion, Kaylea. 2019. “Production Misalignment: A Threat to Public Knowledge.” Master  of Arts Thesis, Seattle, Washington: University of Washington.](https://digital.lib.washington.edu:443/researchworks/handle/1773/45156)
 
 - [Pandas](https://pandas.pydata.org/)
 
 - [Scrapy](https://scrapy.org/)
 
 - [Harvard Dataverse](https://dataverse.harvard.edu/)
 
 - [Snowdrift](https://snowdrift.coop/)
 
 - [SeaGL](https://seagl.org/)

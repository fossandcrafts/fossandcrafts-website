title: 61: A Textile Historian's Survival Guide
date: 2023-12-10 14:25
tags: crafts, textiles
slug: 061-textile-historians-survival-guide
summary: What happens when a textile historian suddenly discovers that she has a medical reason to apply the underlying techniques of her area of study?
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/061-textile-historians-survival-guide.ogg" length:"61927521" duration:"01:13:52"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/061-textile-historians-survival-guide.mp3" length:"70918022" duration:"01:13:52"
---
How do you survive in a world that is no longer optimized for making
your own clothing when you suddenly find that modern conveniences no
longer accommodate you?  As a textile historian, Morgan has been
ruminating for years about women’s contributions to the domestic
economy, the massive time investment of producing clothing for a
family, and the comparative properties of different textile
fibers. These research interests were informed by a lifetime of sewing
and other fiber crafts. None of this experience, however, properly
prepared her to face the reality of needing to rely on her own hands
to provide large portions of her own wardrobe.

Guest co-host Juliana Sims sits down with Morgan to talk about how,
in the wake of a recently developed allergy to synthetic fabrics, she
now finds herself putting that knowledge of historical textile
production to use to produce clothing that she can wear.

**Links and other notes:**
- Morgan presented this as a (much shorter) talk at the [Dress Conference 2023](https://dressconference.org/)
- [Slides from the presentation](/static/images/blog/a-textile-historians-guide-to-making-clothing.pdf)
- [Morgan's Dissertation](https://mlemmer.org/dissertation/), which we [also](https://fossandcrafts.org/episodes/34-women-and-wool-working-part1.html) [covered](https://fossandcrafts.org/episodes/35-women-and-wool-working-part2.html)
- [RSI Glove Pattern](https://mlemmer.org/blog/RSIgloves/)

The quote that Morgan somewhat misremembered about a woman preparing
wool before the winter:

> "A thrifty countrywoman had a small croft, she and her sturdy spouse. He tilled his own land, whether the work called for the plough, or the curved sickle, or the hoe. She would now sweep the cottage, supported on props; now she would set the eggs to be hatched under the plumage of the brooding hen; or she gathered green mallows or white mushrooms, or warmed the low hearth with welcome fire. And yet she diligently employed her hands at the loom, and armed herself against the threats of winter." -- Ovid, Fasti 4.687-714



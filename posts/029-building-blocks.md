title: 29: Building Blocks for User Freedom
date: 2021-06-12 16:10
tags: foss, crafts, presentation
slug: 29-building-blocks-for-user-freedom
summary: Chris and Morgan's talk at ÖzgürKon about building blocks as a lens for fundamental user-emposering tooling
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/029-building-blocks.ogg" length:"35055520" duration:"00:50:57"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/029-building-blocks.mp3" length:"48927518" duration:"00:50:57"
---
Any skillset has basic foundational elements or building blocks. In
this recording of Chris and Morgan's
[talk at ÖzgürKon](https://ozgurkon.org/2021/sessions/webber/),
we discuss the way that access to those basic elements is limited in
modern society.
This can be seen in any number of fields from actual building blocks
increasingly being sold in sets to make specific toys as opposed to
generic buckets of blocks that allow kids to develop their creativity
to the way that access to the source code and hardware in our
technology is increasingly restricted.

Also!  In this episode we announce
[Hack and Craft](https://fossandcrafts.org/hack-and-craft/),
a new companion "stitch and bitch" style usergroup to FOSS & Crafts.
(Any crafting is welcome... including computer programming, as long
as it's a fun project!)
Feel free to bring your own project and hang out at inagural meeting
on June 19th!

**Links:**

 - [ÖzgürKon 2021](https://ozgurkon.org/2021/)

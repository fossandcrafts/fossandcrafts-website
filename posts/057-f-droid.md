title: 57: F-Droid (featuring Sylvia van Os & Hans-Christoph Steiner!)
date: 2023-05-14 21:30
tags: f-droid
slug: 057-f-droid
summary: Christine interviews Morgan, Sylvia, and Hans about F-Droid
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/057-f-droid.ogg" length:"32433226" duration:"00:52:00"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/057-f-droid.mp3" length:"49922691" duration:"00:52:00"
---
F-Droid, a repository of free software for your Android devices!
Christine interviews F-Droid developers Sylvia van Os and
Hans-Christoph Steiner as well as F-Droid board member and chair...
Morgan Lemmer-Webber!

**Links:**

- [F-Droid](https://f-droid.org/)
- [Sylvia van Os](https://sylviavanos.nl/)
- [Hans-Christoph Steiner](https://at.or.at/)
- [F-Droid board announcement](https://f-droid.org/2023/03/20/f-droid-board.html)
- [Guardian Project](https://guardianproject.info/)
- [Google Play bans Matrix/Element](https://arstechnica.com/gadgets/2021/01/google-play-bans-open-source-matrix-client-element-citing-abusive-content/)
- [Catima](https://catima.app/)
- [Your app is not compliant with Google Play Policies: A story from hell](https://sylviavanos.nl/blog/2021/12/24/google_play_hell.html)

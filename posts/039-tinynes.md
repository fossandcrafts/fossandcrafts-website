title: 39: The TinyNES: An Open Hardware "Tiny Nostalgia Evocation Square"
date: 2021-12-16 20:10
tags: open hardware, tinynes, campaign, fandc-studios
slug: 39-tinynes
summary: We talk with Dan Gilbert about the TinyNES: an open hardware gaming console compatible with the Famicom / Nintendo Entertainment System
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/039-tinynes.ogg" length:"55625797" duration:"01:07:45"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/039-tinynes.mp3" length:"65041126" duration:"01:07:45"
---
![The TinyNES!](/static/images/blog/tinynes.jpg)

Dan Gilbert of [Tall Dog](https://www.tall-dog.com/) joins us to talk
about the
[Tiny Nostalgia Evocation Square (or TinyNES for short)](https://www.crowdsupply.com/tall-dog-electronics/tinynes/)!
The TinyNES is an open hardware system compatible with the compatible
with original Nintendo Entertainment System and Famicom cartridges and
controllers.
Instead of being just an emulator or FPGA-based implementation, the
TinyNES uses the original 6502-derived chips and a custom circuit
board, preserving and carrying forward computing history!
Oh yeah, and it's also running a crowdfunding campaign,
so you can [order your own](https://www.crowdsupply.com/tall-dog-electronics/tinynes/)
and support open hardware in the best way possible: by playing video games!

By the way, we mentioned that FOSS & Crafts Studios would be launching
its first collaboration... we're helping to run the crowdfunding
campaign on this one (and couldn't be more excited about it)!

**Links:**

 - [TinyNES crowdfunding campaign](https://www.crowdsupply.com/tall-dog-electronics/tinynes/) ([launch announcement](https://www.crowdsupply.com/tall-dog-electronics/tinynes/updates/crowdfunding-begins), sources will be on [tinynes.com](https://tinynes.com) when campaign succeeds)
 
 - [Tall Dog](https://www.tall-dog.com/), Dan's company (they do some
   other cool open hardware stuff too, check 'em out!)

 - [Tall Dog's statement on supporting open source](https://www.tall-dog.com/open_source.html)
 
 - The [6502 chip](https://en.wikipedia.org/wiki/MOS_Technology_6502)
   and its specially modified version for the Nintendo Enetertainment System, the
   [Ricoh 2A03](https://en.wikipedia.org/wiki/Ricoh_2A03)

 - [FreeCAD](https://www.freecadweb.org/) and [KiCAD](https://www.kicad.org/)

 - [Visual6502](http://www.visual6502.org/)
 
 - [Nova the Squirrel](https://github.com/NovaSquirrel/NovaTheSquirrel)
 
 - [Everdrive](https://krikzz.com/) (proprietary hardware, but lets
   you run custom ROMs, including Nova)

 - [Robot Finds Kitten on the c64!](https://pezi-pink.itch.io/c64-robotfindskitten)
   Written [in Racket](https://docs.racket-lang.org/asi64/index.html)!

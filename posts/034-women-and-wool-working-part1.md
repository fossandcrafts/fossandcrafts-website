title: 34: Women and Wool Working in the Ancient Roman Empire, Part 1
date: 2021-08-19 10:05
tags: crafts, dissertation, academia
slug: 34-women-and-wool-working-part1
summary: Morgan presents the first part of her PhD dissertation
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/034-women-and-wool-working-part1.ogg" length:"52143008" duration:"01:03:13"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/034-women-and-wool-working-part1.mp3" length:"60690167" duration:"01:03:13"
---
In the first of two episodes on
[Morgan's dissertation](https://mlemmer.org/dissertation/)
we introduce the topic of women and textile production in the Roman
Empire. Scholars have often viewed the domestic and commercial divide
in textile production along gendered lines, associating domestic
production with women in the context of the ideal of feminine virtue
and commercial production with men working in centralized production
centers. Here we use the cottage industry model to contextualize the
role of women’s labor in the Roman textile industry, exploring the
links between domestic production and commercial distribution.

**Links:**

 - [Morgan's dissertation](https://mlemmer.org/dissertation/)

 - [Episode 26: Dr. Morgan Lemmer-Webber, an academic journey](https://fossandcrafts.org/episodes/26-dr-mlemweb-academic-journey.html)
   talks about the process of getting a PhD (from Morgan's personal
   experiences, your mileage may vary)

 - [Episode 3: Textile production and a nostalgic past](https://fossandcrafts.org/episodes/3-textile-production-and-a-nostalgic-past.html)
   discusses Augustan propaganda and textile production and gives a
   summary of the stages of textile production from sheep to sweater.

 - The two Augustan versions of the Lucretia myth by
   [Ovid, Fasti 2.722-751](https://www.theoi.com/Text/OvidFasti2.html)
   and [Livy, History of Rome 1.57.9](http://www.perseus.tufts.edu/hopper/text?doc=Perseus%3Atext%3A1999.02.0026%3Abook%3D1%3Achapter%3D57)
   (**Content Warning:** rape, suicide, revolution)

 - Another instance of a woman's labor from Ovid, this time a more
   modest country woman who must weave cloaks etc before winter to
   protect her family from the cold:
   [Ovid, Fasti 4.687-714](https://www.theoi.com/Text/OvidFasti4.html).

 - Hitchner, Robert Bruce. 2012. "Olive Production and the Roman
   Economy: The Case for Intensive Growth in the Roman Empire." In
   *The Ancient Economy,* Taylor and Francis. Partial text available
   on [Google Scholar](https://books.google.com/books?id=ZCop2f6td4kC&lpg=PA71&ots=N1nHwafFn3&lr&pg=PA71#v=onepage&q&f=false).

 - Barber, Elizabeth. 1994. *Women’s Work: the First 20,000 Years:
   Women, Cloth, and Society in Early Times.* New York : Norton.

 - [Lena Larsson Lovén](https://gu-se.academia.edu/LenaLarssonLov%C3%A9n)
   has written extensively on both the iconography of textile
   production and the performative relationships between women and
   wool work in the Roman Empire.

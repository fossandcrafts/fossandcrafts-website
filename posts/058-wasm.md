title: 58: WebAssembly
date: 2023-06-15 20:10
tags: wasm, foss
slug: 058-wasm
summary: WebAssembly: towards all languages in the browser!
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/058-wasm.ogg" length:"23127289" duration:"00:24:05"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/058-wasm.mp3" length:"19708184" duration:"00:24:05"
---
WebAssembly!  You've probably heard lots about it, but what the heck
is it?  Is it just for C and Rust programs?  Can you write it by hand?
(Do you want to?)  And wait, how is Spritely getting involved in
WebAssembly efforts?  Find out!

**Links:**

- [WebAssembly](https://webassembly.org/)
- [Hoot!](https://gitlab.com/spritely/guile-hoot/) (and [Hoot announcement](https://spritely.institute/news/guile-on-web-assembly-project-underway.html), [Andy Wingo joining](https://spritely.institute/news/andy-wingo-leads-g2W.html), [Robin Templeton joining](https://spritely.institute/news/robin-templeton-joins.html))
- [Lisp Game Jam - "Wireworld" - Hoot's low level WASM tooling in action](https://spritely.institute/news/hoot-wireworld-live-in-browser.html)
- [Directly compiling Scheme to WebAssembly: lambdas, recursion, iteration!](https://spritely.institute/news/scheme-to-wasm-lambdas-recursion.html)
- [Understanding the WebAssembly text format](https://developer.mozilla.org/en-US/docs/WebAssembly/Understanding_the_text_format)
- [WebAssembly GC proposal](https://github.com/WebAssembly/gc/blob/master/proposals/gc/Overview.md)
- [Episode 49: Lisp but Beautiful; Lisp for Everyone](https://fossandcrafts.org/episodes/49-lisp-but-beautiful-lisp-for-everyone.html)
- [WASI](https://wasi.dev/)
- [POSIX](https://en.wikipedia.org/wiki/POSIX)
- [Episode 17: Gardening, from seedling to seasoned](https://fossandcrafts.org/episodes/17-gardening-seedling-to-seasoned.html)
- [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)
- [WASM-4](https://wasm4.org/)
- [Episode 46: Mark S. Miller on Distributed Objects, Part 1](https://fossandcrafts.org/episodes/46-mark-miller-on-distributed-objects-part-1.html)
- [Schism](https://github.com/google/schism) by Eric Holk



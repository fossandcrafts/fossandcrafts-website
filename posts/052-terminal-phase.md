title: 52: Terminal Phase: a space shooter that runs in your terminal!
date: 2022-11-13 17:05
tags: foss, crafts, games, terminal phase, spritely, goblins
slug: 052-terminal-phase
summary: We talk about the making of Christine's project Terminal Phase, a space shooter that runs in your terminal!
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/052-terminal-phase.ogg" length:"20463540" duration:"00:23:24"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/052-terminal-phase.mp3" length:"22476994" duration:"00:23:24"
---
[![Terminal Phase](/static/images/blog/terminal-phase-parallax-starfield.gif)](https://gitlab.com/dustyweb/terminal-phase)

[Terminal Phase!](https://gitlab.com/dustyweb/terminal-phase)
A space shooter that runs in your terminal!!!
Who wouldn't be excited about that?

Not to mention that it shows off cool features of
[Spritely Goblins](https://spritely.institute/goblins/)...
like time travel:

[![Terminal Phase](/static/images/blog/terminal-phase-goblins-time-travel.gif)](https://dustycloud.org/blog/goblins-time-travel-micropreview/)

Well, Terminal Phase has been Christine's fun/downtime project for the
last few years, *and* one of the bonuses you can get for the reward
tiers of donating to this podcast!  And yet we've never done an
episode about it!  Given that a brand new (and much easier to install)
release of Terminal Phase is coming out really soon, we figured now's
a good time to talk about it!

**Links:**

- [Terminal Phase!](https://gitlab.com/dustyweb/terminal-phase)
- Blogposts about Terminal Phase!
  - [Project announcement](https://dustycloud.org/blog/terminal-phase-prototype/)
  - [1.0 announcement](https://dustycloud.org/blog/terminal-phase-1.0/)
  - [Time travel debugging in Spritely Goblins, previewed through Terminal Phase](https://dustycloud.org/blog/goblins-time-travel-micropreview/)
  - [1.1 announcement](https://dustycloud.org/blog/terminal-phase-1.1-and-goblins-0.6/)
  - [Terminal Phase was in a Polish "Linux magazine"!](https://dustycloud.org/blog/terminal-phase-in-linux-magazine-pl/)
- [FOSS & Crafts' Patreon](https://www.patreon.com/fossandcrafts)
- [Spritely Goblins](https://spritely.institute/goblins/), a project of the [Spritely Institute](https://spritely.institute/)
- [Blast off! A tour of Spritely Institute's tech](https://spritely.institute/news/blast-off-spritely-institutes-tech-tour.html)
- [Racket](https://racket-lang.org/)
- [Guile](https://www.gnu.org/software/guile/)
- [Guix](https://guix.gnu.org/)
- [8sync](https://www.gnu.org/software/8sync/) (Goblins predecessor).  See also the Mudsync video, on that very page.
- [Raart](https://docs.racket-lang.org/raart/index.html)
- [Spacewar!](https://en.wikipedia.org/wiki/Spacewar!)
- [A bit about how Spacewar lead to UNICS (later renamed Unix)](https://unix.org/what_is_unix/history_timeline.html)

title: 20: Hygiene for a computing pandemic
date: 2021-01-03 17:40
tags: ocaps, security, pandemic, medical, foss
slug: 20-hygiene-for-a-computing-pandemic
summary: Chris and Morgan, driving in the Covid-19 pandemic, reflect on lessons of hygiene and a separation of concerns from the past (with the retroactively surprising struggle for handwashing acceptance) while analyzing hygiene for helping bring safety to a computing pandemic of today via object capability discipline.
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/020-hygiene-for-a-computing-pandemic.ogg" length:"66902273" duration:"01:19:53"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/020-hygiene-for-a-computing-pandemic.mp3" length:"76697977" duration:"01:19:53"
---
Chris and Morgan, driving in the Covid-19 pandemic, reflect on lessons
of hygiene and a separation of concerns from the past (seen through
the retroactively surprising struggle for handwashing acceptance)
while analyzing how to bring safety to today's computing security
pandemic via object capability discipline.

As said in the episode, there's a lot of research and evidence for
the object capability security approach!
Please do scour the links below (with significant commentary attached).

**Links:**

 - [Ignaz Semmelweis](https://en.wikipedia.org/wiki/Ignaz_Semmelweis)
   and two excellent podcast episodes with more:

   - [Ignaz Semmelweis and the War on Handwashing](https://www.iheart.com/podcast/stuff-you-missed-in-history-cl-21124503/episode/ignaz-semmelweis-and-the-war-on-29118226/) on [Stuff You Missed in History Class](https://www.iheart.com/podcast/stuff-you-missed-in-history-cl-21124503/)

   - [The fascinating, inspiring, and infurating story of Ignaz Semmelweis](https://maximumfun.org/episodes/sawbones/sawbones-ignaz-semmelweis/) on [Sawbones](https://maximumfun.org/episodes/sawbones/sawbones-ignaz-semmelweis/)

 - The mailing list post by Chris that prompted this episode (largely the same stuff, a bit more particular to the targeted audience): [Hygiene for a computing pandemic: separation of VCs and ocaps/zcaps](https://lists.w3.org/Archives/Public/public-credentials/2020Dec/0028.html)

 - [POLA Would Have Prevented the Event-Stream Incident](https://medium.com/agoric/pola-would-have-prevented-the-event-stream-incident-45653ecbda99), by Kate Sills.  Examines how malicious code inserted into a library designed to steal programmers' private information/keys/money could have been prevented with capability-based security.

 - [An interview with Kate Sills about object capabilities](https://librelounge.org/episodes/episode-13-object-capabilities-with-kate-sills.html); contains some of the same information presented in this episode, but with more focus on the basic concepts.

 - [A Security Kernel based on the Lambda Calculus](https://lists.w3.org/Archives/Public/public-credentials/2020Dec/0028.html) explains how these concepts apply to programming language design (using a limited subset of the Scheme programming language).

 - Ka-Ping Yee's PhD dissertation, [Building Reliable Voting Machine Software](https://web.archive.org/web/20200702023836/http://zesty.ca/pubs/yee-phd.pdf), demonstrates the difficulty of finding intentionally obscured security vulnerabilities through code review (see "How was PVote's security evaluated?").  This demonstrates that FOSS is *necessary but insufficient on its own* for security.

 - A [backdoor which was inserted into the official Linux kernel source code](https://lwn.net/Articles/57135/) (and actually distributed on the official CVS server, briefly!) all the way back in 2003.  Note that the vulnerability was initially discovered not through code review, but through discovering a server intrusion.  The code is well obfuscated in a way that might be difficult to observe through visual inspection of a significant body of code.

 - The [zcap-ld spec](https://w3c-ccg.github.io/zcap-ld/) has a subsection on [how to safely and hygienically bridge the worlds of identity/claims/credentials with authority/ocaps](https://w3c-ccg.github.io/zcap-ld/#relationship-to-vc).  (Note some bias here: Chris co-authored this spec with Mark Miller.)  It also has some other useful subsections: [Capabilities are Safer](https://w3c-ccg.github.io/zcap-ld/#capabilities-are-safer) contrasts with ACLs, and [ZCAP-LD by Example](https://w3c-ccg.github.io/zcap-ld/#zcap-by-example) shows how capabilities can be constructed on top of certificate chains (an approach not even mentioned in the episode... but yes, you can do it!)

 - So why are ACLs / an identity-oriented approach so bad anyway?  [ACLs Don't](http://waterken.sourceforge.net/aclsdont/current.pdf) explains the problems caused by an identity-oriented authority model:

   - [Ambient authority](https://en.wikipedia.org/wiki/Ambient_authority), ie "programs running with too much authority"... think about the "solitaire running 'as you'" part of the podcast (and contrast with the POLA/ocap solution also explained in-episode)

   - [Confused deputies](https://en.wikipedia.org/wiki/Confused_deputy_problem), which are notoriously kind of hard to describe... Norm Hardy provides a [capsule summary](http://www.cap-lore.com/CapTheory/CD.html) which is fairly good.  But also:
 
     - [The Browser is a very Confused Deputy](https://www.youtube.com/watch?v=Yfsmc0b8o78) is an excellent and fun video introduction.

     - Norm Hardy's original [Confused Deputy paper](http://www.cap-lore.com/CapTheory/ConfusedDeputy.html) is still worth reading, and there is [more to read here](http://www.cap-lore.com/CapTheory/ConfusedDeputyM.html)

     - An example of a confused deputy attack against the Guile programming environment (which Chris helped uncover): [Guile security vulnerability w/ listening on localhost + port (with fix)](https://lists.gnu.org/archive/html/guile-user/2016-10/msg00007.html).  Note the way that both the browser and the guile programming environment appear to be "correctly behaving according to specification" when looked at individually!

     - Another way to put it is that identity-oriented security approaches are also generally *perimeter-based* security approaches and (I'm [paraphrasing Marc Stiegler here](http://www.skyhunter.com/marcs/ewalnut.html#SEC44)): "Perimeter security is eggshell security... it seems pretty tough when you tap on it, but poke one hole through and you can suck out the whole yolk."

 - [Capabilities: Effects for Free](http://www.cs.cmu.edu/~aldrich/papers/effects-icfem2018.pdf) shows nicely how capabilities can also be combined with a type system to prove constraints on what a particular subset of code can do.

 - What we haven't talked about as much yet is all the cool things that ocaps *enable*.  A great paper on this is [Capability-based Financial Instruments](http://erights.org/elib/capability/ode/index.html) (aka "Ode to the Granovetter Diagram", or "The Ode"), which shows how, using the [E distributed programming language](http://erights.org/index.html), distributed financial tooling can be built out of a shockingly small amount of code.  (All of this stuff written about a decade before blockchains hit the mainstream!)

 - You might need to know a bit more E syntax to read The Ode; Marc Stiegler's [E in a Walnut](http://www.skyhunter.com/marcs/ewalnut.html) is an *incredible* resource, and has many insights of its own... but it's a bit more coconut-sized than walnut-sized, in my view.

 - An enormous amount of interesting information and papers about object capability security on the [E Wiki](http://wiki.erights.org)'s [Documentation page](http://wiki.erights.org/wiki/Documentation) page ([snapshot](https://web.archive.org/web/20200918043946/http://wiki.erights.org/wiki/Documentation)).  Honestly you could just spend a few months reading all that.

 - In particular, if you're mathematically minded and say "yeah but I want the proofs, gimme the proofs; I mean like real math'y proofs!" there's a whole subsection on [Formal Methods](http://wiki.erights.org/wiki/Documentation#Formal_Methods) ([snapshot](https://web.archive.org/web/20200918043946/http://wiki.erights.org/wiki/Documentation#Formal_Methods))

 - But maybe you're worrying, is it possible to build secure UIs on top of this?  [Not One Click for Security](https://www.hpl.hp.com/techreports/2009/HPL-2009-53.html) does a lovely job showing how ocap principles can actually result in a *more intuitive* flow if done correctly... one smooth enough that users might wonder, "where's the security?"  Surprise!  It was just smoothly baked into the natural flow of the application, which is why you didn't notice it!

 - And if you really want to spend a lot of time getting into the weeds of how to *design* ocap systems, maybe look at Mark S. Miller's PhD dissertation, [Robust Composition: Towards a Unified Approach to Access Control and Concurrency Control](http://www.erights.org/talks/thesis/).  Chris is pretty sure they're the only one with an autographed copy sitting on their desk.

 - Finally, have we mentioned that Chris's work on [Spritely](https://spritelyproject.org/) is pretty much entirely based on extending the federated social web based on ocap security principles?

title: 59: Governance, part 1
date: 2023-08-31 21:40
tags: foss, governance
slug: 059-governance-part1
summary: Governance part 1: general governance considerations
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/059-governance-part1.ogg" length:"32331533" duration:"00:49:49"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/059-governance-part1.mp3" length:"45883921" duration:"00:49:49"
---
Governance of FOSS projects, a two parter, and this is part one!
Here we talk about general considerations applicable to FOSS projects!
(And heck, these apply to collaborative free culture projects too!)

**Links:**

- [Why We Need Code of Conducts, and Why They're Not Enough, by Aeva Black](https://www.youtube.com/watch?v=1GiwoMkuSHg&t=8160s)
- [Blender Cloud](https://cloud.blender.org) and the [Blender Development Fund](https://fund.blender.org/)

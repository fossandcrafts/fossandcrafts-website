title: 60: Governance, part 2
date: 2023-09-30 23:10
tags: foss, governance
slug: 060-governance-part2
summary: Governance part 2: some governance "templates"
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/060-governance-part2.ogg" length:"27392376" duration:"00:40:42"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/060-governance-part2.mp3" length:"39078291" duration:"00:40:42"
---
Back again with governance... part two!
(See also: [part one](https://fossandcrafts.org/episodes/059-governance-part1.html)!)
Here we talk about some organizations and how they can be seen as
"templates" for certain governance archetypes.

**Links:**

- [Cygnus](https://en.wikipedia.org/wiki/Cygnus_Solutions), [Cygwin](https://www.cygwin.com/)
- [Mastodon](https://joinmastodon.org/)
- [Android](https://www.android.com/)
- [Free Software Foundation](https://www.fsf.org/), [GNU](https://www.gnu.org/)
- [Software Freedom Conservancy](https://sfconservancy.org/), [Outreachy](https://www.outreachy.org/), [Conservancy's copyleft compliance projects](https://sfconservancy.org/copyleft-compliance/)
- [Commons Conservancy](https://commonsconservancy.org/)
- [F-Droid](https://f-droid.org/)
- [Open Collective](https://opencollective.com/)
- [Linux Foundation](https://www.linuxfoundation.org/)
- [501(c)(3)](https://en.wikipedia.org/wiki/501(c)(3)_organization) vs [501(c)(6)](https://en.wikipedia.org/wiki/501(c)_organization#501.28c.29.286.29)
- [Stitchting](https://en.wikipedia.org/wiki/Stichting)
- [Free as in Freedom](http://faif.us)
- [LKML](https://lkml.org/) (the Linux Kernel Mailing List)
- [Linus Doesn't Scale](https://web.archive.org/web/20020214033203/http://www.kerneltrap.com/article.php?sid=513)
- [Spritely Networked Communities Institute](https://spritely.institute)
- [Python](https://www.python.org/) and the [Python Software Foundation](https://www.python.org/psf-landing/), [PyCon](https://us.pycon.org/), the [Python Package Index](https://pypi.org/)
- [Python PEPs (Python Enhancement Proposals)](https://peps.python.org/pep-0000/), [XMPP XEPs](https://xmpp.org/extensions/), [Fediverse FEPs](https://codeberg.org/fediverse/fep), [Rust RFCs](https://github.com/rust-lang/rfcs)
- [Blender](https://www.blender.org/), [Blender Foundation](https://www.blender.org/about/foundation/), [Blender Institute](https://www.blender.org/about/institute/), [Blender Studio](https://studio.blender.org/welcome/)
- [Blender's history](https://www.blender.org/about/history/)
- [Elephants Dream](https://orange.blender.org/)
- [Mozilla Foundation](https://foundation.mozilla.org/en/) and [Mozilla Corporation](https://www.mozilla.org/en-US/foundation/moco/)
- [Debian](https://www.debian.org/), [Debian's organizational structure](https://www.debian.org/intro/organization), and [Debian's constitution](https://www.debian.org/devel/constitution)
- [EFF](https://www.eff.org/)
- Oh yeah and I guess we should link the [World History Association](https://www.thewha.org/)!

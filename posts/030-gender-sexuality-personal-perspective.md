title: 30: Gender and Sexuality, A Personal Perspective
date: 2021-06-27 21:05
tags: gender, sexuality, lgbtqia, nonbinary, transgender, demisexual, pansexual
slug: 30-gender-sexuality-personal-perspective
summary: Chris talks about being nonbinary trans-femme and Morgan talks about being demisexual
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/030-gender-sexuality-personal-perspective.ogg" length:"58747219" duration:"01:09:32"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/030-gender-sexuality-personal-perspective.mp3" length:"66759337" duration:"01:09:32"
---
On this episode, Chris talks about being nonbinary trans-femme and
Morgan talks about being demisexual (and briefly about both being
pansexual) and how they have both navigated these experiences in their
lives and relationship.

![A picture of Chris and Morgan together](https://dustycloud.org/gfx/goodies/chris-and-morgan-2021-06-27.jpg)

**Links:**

 - There are a lot of resources on the internet about being
   transgender and nonbinary, and opinions about most of them tend to
   run strong.  That said,
   [Transgender Map](https://www.transgendermap.com/) has good
   resources explaining many concerns for those who are transgender
   or nonbinary, are trying to figure out if they are transgender or
   nonbinary, and support materials for family and friends.
   
 - [demisexuality.org](http://demisexuality.org/)

 - [WebMD article on demisexuality](https://www.webmd.com/sex/what-is-demisexual-demisexuality).
   Note that despite what Chris says on the episode, the page itself
   does not mention narrative components of demisexual attraction, but
   rather emotional ones.  The extrapolation of application to narrative
   aspects came more from Morgan and Chris talking through Morgan's
   manifestation of those aspects.

 - Chris's "Alpha Release" post about being nonbinary trans-femme with
   pictures,
   [on the fediverse](https://octodon.social/@cwebber/106411792284165142)
   and
   [on Twitter](https://twitter.com/dustyweb/status/1404588591175671813)
   (and [later](https://octodon.social/@cwebber/106417338077038275)
   [update](https://twitter.com/dustyweb/status/1404944814890700803)).
   (These were the first pictures Chris took in a more directly "femme"
   gender expression and do not reflect the current state of the
   development branch.)
   
 - Morgan's coming out thread about being demisexual,
   [on the fediverse](https://octodon.social/@mlemweb/106444731690249184)
   and
   [on Twitter](https://twitter.com/mlemweb/status/1406695303965822982).

 - That [sketchover self-portrait](https://dustycloud.org/gfx/goodies/self-portrait-sketchover.png)
   that Chris mentioned ([overlaid over photo](https://dustycloud.org/gfx/goodies/self-portrait-sketchover-withphoto.png))

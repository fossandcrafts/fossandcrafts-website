title: 49: Lisp but Beautiful; Lisp for Everyone
date: 2022-07-14 21:40
tags: foss, lisp
slug: 49-lisp-but-beautiful-lisp-for-everyone
summary: Talk from FOSDEM about how to make Lisp accessible to a broader audience
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/049-lisp-but-beautiful-lisp-for-everyone.ogg" length:"25309145" duration:"00:37:24"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/049-lisp-but-beautiful-lisp-for-everyone.mp3" length:"35916868" duration:"00:37:24"
---
![Dreaming of a structured wisp future, from the talk](/static/images/blog/wisp-dream-of-shapes.png)

Morgan's out sick!  And yet Morgan is still in this episode!
And that's because this episode is the audio version of
[a talk by the very same name from FOSDEM 2022](https://fosdem.org/2022/schedule/event/lispforeveryone/),
co-presented by Christine and Morgan!
But since Morgan isn't here, Christine fills in, and also gets
a bit silly.

**HACK AND CRAFT SCHEME TUTORIALS!**
The last live scheme tutorial went really well!
And relatedly, Christine and the Spritely Institute just published
[A Scheme Primer](https://spritely.institute/static/papers/scheme-primer.html),
which is more or less the text version of that presentation!
The next live verison of the sheme tutorial will be hosted at
[Hack & Craft](https://fossandcrafts.org/hack-and-craft/)!
Come this Saturday,
**July 16, 2pm-4pm ET (6pm-8pm UTC)!**
We're planning to record this one!

Oh, and bonus Fructure gif:

[![Fructure in action!](/static/images/blog/fructure-rounded-modified.gif)](https://github.com/disconcision/fructure)

**Links:**

 - The [video version of this talk](https://fosdem.org/2022/schedule/event/lispforeveryone/)
 
 - [Episode 47: What is Lisp?](https://fossandcrafts.org/episodes/47-what-is-lisp.html)
 
 - [Wisp](https://www.draketo.de/software/wisp) and its associated
   [SRFI-119](https://srfi.schemers.org/srfi-119/srfi-119.html)
   
 - [Fructure](https://github.com/disconcision/fructure)!!!
   Watch the [amazing RacketCon talk!](https://www.youtube.com/watch?v=CnbVCNIh1NA)

title: 38: Spritely Updates! (November 2021)
date: 2021-11-28 12:15
tags: foss, spritely, updates
slug: 38-spritely-updates-november-2021
summary: Christine and Morgan talk about the updates to the Spritely project, the decentralized networking environment project founded by Christine
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/038-spritely-update.ogg" length:"21268536" duration:"00:24:53"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/038-spritely-update.mp3" length:"23894685" duration:"00:24:53"
---
It's time for some updates on [Spritely](https://spritelyproject.org/), the project Christine founded to advance decentralized networking technology!  A lot has happened since our [episode about Spritely from last year](https://fossandcrafts.org/episodes/9-what-is-spritely.html) (which is really where Spritely got its main public announcement)!  Most notably, [Jessica Tallon has joined the project](https://spritelyproject.org/news/interview-with-jessica-tallon.html) thanks to a generous grant from [NLNet](https://nlnet.nl/PET/) and [NGI Zero](https://www.ngi.eu/ngi-projects/ngi-zero/)!  But there's a lot more that has happened too, so listen in!

ALSO!  As mentioned at the end of this episode, starting with the NEXT episode, we'll begin signing off every episode by thanking [donors to FOSS & Crafts Studios' Patreon](https://www.patreon.com/fossandcrafts)!  By donating you both support this podcast AND Christine's work on Spritely!

**Links:**

 - [The Spritely Project](https://spritelyproject.org/)

 - [FOSS & Crafts Studios' Patreon](https://www.patreon.com/fossandcrafts)!  Donate to show up in the thank-yous for upcoming episodes!

 - The previous ["What is Spritely?"](https://fossandcrafts.org/episodes/9-what-is-spritely.html) of this podcast

 - [Jessica Tallon joins with a grant from NLNet/NGI Zero! Plus an interview!](https://spritelyproject.org/news/interview-with-jessica-tallon.html)
 
 - [Spritely Brux](https://spritelyproject.org/#brux), Spritely's identity and trust management framework, which Jessica is working on (and Morgan dressed as for the costume contest)

 - [Goblin-Chat](https://gitlab.com/spritely/goblin-chat) (mostly a prototype to demonstrate the underlying networking tech)
 
 - [Spritely Goblins](https://spritelyproject.org/#goblins), Spritely's distributed programming environment framework (and which Christine dressed as for the costume contest) ([code](https://gitlab.com/spritely/goblins), [documentation](https://docs.racket-lang.org/goblins/index.html))
 
 - Work in progress port of [Goblins on Guile](https://gitlab.com/spritely/guile-goblins)!  It's getting close!
 
 - [Spritely Aurie](https://gitlab.com/spritely/aurie), Spritely's security-preserving runtime serialization and upgrade framework
 
 - [Safe Serialization Under Mutual Suspicion](http://erights.org/data/serial/jhu-paper/index.html) by Mark S. Miller
 
 - [Pickling, Uneval, Unapply](https://odontomachus.wordpress.com/2020/12/09/pickling-uneval-unapply/) by Jonathan Rees

 - [OCapN](https://github.com/ocapn/ocapn), the new generation of [CapTP](http://erights.org/elib/distrib/captp/index.html) and friends (see also [What is CapTP, and what does it enable?](https://spritelyproject.org/news/what-is-captp.html))
 
 - [Coroutines](https://en.wikipedia.org/wiki/Coroutine), [Goblins' scoped suport for them](https://gitlab.com/spritely/goblins/-/blob/master/goblins/actor-lib/await.rkt).  As for why they aren't prioritized in Goblins, read up on [re-entrancy attacks](https://quantstamp.com/blog/what-is-a-re-entrancy-attack), including this [ancient e-lang email thread](https://web.archive.org/web/20070504043534/http://www.eros-os.org/pipermail/e-lang/2001-July/005418.html)

 - Goblins' integration with Racket's asynchronous programming stuff via [sync/pr](https://gitlab.com/spritely/goblins/-/blob/master/goblins/actor-lib/sync-pr.rkt) (will be [documented in the next tutorial version](https://gitlab.com/spritely/goblins/-/commit/40056e4328bbba11e773dead15b714c23923ec18))

 - [SeaGL](https://seagl.org), where [Morgan and Christine keynoted](https://osem.seagl.org/conferences/seagl2021/program/proposals/866)... and performed in the [costume contest](https://osem.seagl.org/conferences/seagl2021/program/proposals/874) as the Spritely Brux and Goblins mascots!
 
![Morgan and Christine dressed as the Brux and Goblins mascots respectively](/static/images/blog/brux-and-goblins-costumes.jpg)

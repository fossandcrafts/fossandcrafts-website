title: 42: Learning the Sewing Machine
date: 2022-03-06 18:00
tags: crafts, sewing
slug: 42-learning-the-sewing-machine
summary: Christine finally overcomes her fear of the sewing machine and we talk about our respective experiences learning it
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/042-learning-the-sewing-machine.ogg" length:"52940919" duration:"01:01:11"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/042-learning-the-sewing-machine.mp3" length:"58747461" duration:"01:01:11"
---
![Christine using the sewing machine](/static/images/blog/christine-sewing.jpg)

Christine finally overcomes her fear of the
[sewing machine](https://en.wikipedia.org/wiki/Sewing_machine)
and we talk about Christine and Morgan's respective experiences
learning it, and how you can pick it up too!

**Links:**

 - Morgan's article on
   [Basic Sewing Patterns](https://mlemmer.org/blog/baskic_sewing_tutorial/).
   Includes pictures of the dicebag and skirt!
   (More tutorials coming soon!)

 - You probably know what a
   [sewing machine](https://en.wikipedia.org/wiki/Sewing_machine) is,
   but isn't there always more to learn?

 - [A Cultural Perspective on Gender Diversity in Computing](https://www.cs.cmu.edu/~lblum/PAPERS/CrossingCultures.pdf)
   and [Building an Effective Computer Science Student Organization: The Carnegie Mellon Women@SCSAction Plan](https://www.cs.usfca.edu/~wolber/DigitalAccess/CMUWomen.pdf)
   (On that note, when Christine was in college she attended a
   presentation by Lenore Blum about women in CS, which is where the
   potluck anecdote comes from.)

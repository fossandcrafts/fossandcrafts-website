title: 53: Fediverse reflections while the bird burns
date: 2022-12-01 14:30
tags: foss, activitypub, fediverse
slug: 053-fediverse-reflections-while-the-bird-burns
summary: We talk about the rise of the federated social web as Twitter burns to the ground.  Is the fediverse ready?
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/053-fediverse-reflections-while-the-bird-burns.ogg" length:"32113555" duration:"00:37:30"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/053-fediverse-reflections-while-the-bird-burns.mp3" length:"36007569" duration:"00:37:30"
---
Twitter is burning, and people are flocking to the fediverse.  Is the
fediverse ready though?  How did we get here?  Where should we be
going?  Since Christine is co-author of ActivityPub, the primary
protocol used by the fediverse, Morgan decides it's time to get
Christine's thoughts recorded and out there... so we hop in the car as
we talk all about it!

**Links:**

 - [ActivityPub](https://www.w3.org/TR/activitypub/), the protocol
   which wires the federated social web together, of which Christine
   is co-author!  Be sure to check out the
   [Overview section](https://www.w3.org/TR/activitypub/#Overview)...
   it's actually fairly easy to understand!

 - Some of the implementations discussed (though there are many more):
   - [Mastodon](https://joinmastodon.org/)
   - [Peertube](https://joinpeertube.org/)
   - [Pixelfed](https://pixelfed.org/)
   - [Pleroma](https://pleroma.social/)

 - A lot has been written about Elon Musk's takeover of Twitter.
   Here's [a pretty decent timeline](https://abcnews.go.com/Business/timeline-elon-musks-tumultuous-twitter-acquisition-attempt/story?id=86611191)
   (though it's missing the [transphobia](https://www.mercurynews.com/2022/10/10/elon-musk-says-he-lost-transgender-daughter-because-of-neo-marxists/) [stuff](https://slate.com/technology/2022/11/elon-music-twitter-transgender-harassment-misinformation.html)).

 - [W3C Social Web Working Group](https://www.w3.org/wiki/Socialwg/)
   is where ActivityPub was standardized

 - [OcapPub](https://gitlab.com/spritely/ocappub/blob/master/README.org)
   (while not complete, it lays out a lot of the core problems with
   the way the fediverse has gone)

 - [The Spritely Institute](https://spritely.institute/)

 - Previous episodes on Spritely: [What is Spritely?](https://fossandcrafts.org/episodes/9-what-is-spritely.html), [Spritely Updates! (November 2021)](https://fossandcrafts.org/episodes/38-spritely-updates-november-2021.html), and sorta kinda the [Terminal Phase episode](https://fossandcrafts.org/episodes/052-terminal-phase.html)

 - [The Presentation of Self on a Decentralized Web](https://dr.amy.gy/) (PhD dissertation by ActivityPub co-author Amy Guy, partly covers its standardization)

 - [SMTP](https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol) and [XMPP](https://en.wikipedia.org/wiki/XMPP) can be seen as decentralized "social networks" before that term took off

 - [OStatus](https://en.wikipedia.org/wiki/OStatus)

 - [pump.io](http://pump.io/) is where the [pump.io API](https://github.com/e14n/pump.io/blob/master/API.md) came from, which is the direct predecessor to ActivityPub

 - [StatusNet / GNU Social](https://en.wikipedia.org/wiki/GNU_social)

 - [Diaspora](https://diaspora.social/)

 - [MediaGoblin](https://mediagoblin.org/)

 - [APConf videos](https://conf.tube/video-channels/apconf_channel/videos)

 - [Context Collapse](https://www.zephoria.org/thoughts/archives/2013/12/08/coining-context-collapse.html)

 - Early writeups from Christine some of these ideas, but are old:

   - [ActivityPub: from decentralied to distributed social networks](https://github.com/WebOfTrustInfo/rwot5-boston/blob/master/final-documents/activitypub-decentralized-distributed.md)

   - [magenc](https://gitlab.com/dustyweb/magenc/blob/master/magenc/scribblings/intro.org)

   - [crystal](https://gitlab.com/spritely/crystal/blob/master/crystal/scribblings/intro.org)

   - [golem](https://gitlab.com/spritely/golem/blob/master/README.org)

title: 22: Crafting the past... or trying to
date: 2021-01-29 17:52
tags: academia, crafts, history, archaeology, anthropology
slug: 22-crafting-the-past
summary: Morgan and Chris explore ways of crafting the past through experiential historical crafts, experimental archaeology, and historical reenactment.
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/022-crafting-the-past.ogg" length:"53226792" duration:"01:04:16"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/022-crafting-the-past.mp3" length:"61699512" duration:"01:04:16"
---
There's all sorts of reasons to pursue historical crafting
techniques: for the experience of recreating them or learning new
techniques, for education, or for entertainment and immersion.
Morgan and Chris explore these paths under the terms "experiential
historical crafts", "experimental archaeology", and "historical
reenactment".
What is important, useful, and fun about each of these?
What pitfalls might we want to avoid?
What can be gained by what we might find, how might we bring more
people in... and what do we risk by what (or who) we might miss or
leave out?

**Links and references:**

 - [Colonial Williamsburg](https://www.colonialwilliamsburg.org/)

 - [Afroculinaria](https://afroculinaria.com/), Michael Twitty’s blog

 - Twitty, Michael. The Cooking Gene : a Journey through African
   American Culinary History in the Old South. New York, NY :Amistad,
   an imprint of HarperCollinsPublishers, 2017.

 - Outram, Alan K. “Introduction to Experimental Archaeology.” World Archaeology, vol. 40, no. 1, 2008, pp. 1–6.

 - [Janet Stephens’s YouTube channel](https://www.youtube.com/channel/UCboS0faGVeMi3n5_2LsVazw), with tutorials for re-creating historical hairstyles

 - Stephens, Janet. “Recreating the Fonseca Hairstyle.” EXARC, 2013/1, https://exarc.net/issue-2013-1/at/recreating-fonseca-hairstyle

 - The journal [EXARC](https://exarc.net/) is a peer-reviewed online journal for experimental archaeology with articles released under CC BY-NC-SA

 - Strand, Eva B. A, Marie-Louise Nosch, and Joanne Cutler. Tools, Textiles and Contexts: Investigating Textile Production in the Aegean and Eastern Mediterranean Bronze Age. Oxford: Oxbow Books, 2015.

 - [Society for Creative Anachronism/SCA](https://www.sca.org)

 - [New Yorker* article about the Townsends episode on “Orange Fool”](https://www.newyorker.com/culture/rabbit-holes/the-eighteenth-century-custard-recipe-that-enraged-trump-supporters) (* Not the New York Times, as Chris misspoke in the podcast)

 - [Townsends episode on “Orange Fool”](https://www.youtube.com/watch?v=T2AG545WIsg )

 - [Townsends episode in aftermath of the “Orange Fool” outrage](https://www.youtube.com/watch?v=YIi1bjl_iqE) (where he specifically states that his channel does not link historical topics to modern politics)

 - [Michael Twitty making Kush on Townsends](https://www.youtube.com/watch?v=kvjsli7ICrI) (“Exactly how you expect stuffing to smell … this is what you expect it to taste like”)

 - [Michael Twitty making Akara on Townsends](https://www.youtube.com/watch?v=ELtVi9ZrvAA) (the fritter/falafel-like dish)

Special note here: we aren't saying Townsends is bad; we enjoy the
show and from a standpoint of production, what it does present is
very good.  But it does seem like the show makes an intentional dodge
on important issues or chooses to only present a limited and fun
subset of history...  which can be disappointing at the least and at
the worst can result in a kind of nostalgia that erases real problems.
All history is suffused with things to celebrate and things which are
disturbing and disappointing, but recognizing only the former sets us
up to repeat the latter.

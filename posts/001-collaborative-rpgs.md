title: 1: Collaborative Storytelling with Dice
date: 2020-07-16 15:55
tags: episode
summary: Collaborative storytelling via narrative role playing games
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/001-collaborative-rpgs.ogg" length:"20141708" duration:"00:29:51"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/001-collaborative-rpgs.mp3" length:"28655229" duration:"00:29:51"
---
You've probably heard of "tabletop Role Playing Games" (or, tabletop
RPGs) before, but what are they?
In this episode, Chris and Morgan introduce a subset of RPGs called
"Narrative RPGs" whose mechanics are focused primarily around
storytelling (as opposed to tactical combat).2
Hear about how narrative RPGs can be used as "collaborative
storytelling with dice", some of the narrative RPG systems that exist,
as well as an in-depth look at one particular RPG system,
[Freeform Universal](http://freeformuniversal.com/).
Freeform Universal is so simple and easy to pick up that by the end of
this episode, you should have enough information to use it for weaving
stories with your friends!

**Links:**

 - Freeform Universal (or FU, pronounced "Foo")
   - [Official FU site](http://freeformuniversal.com/)
   - [Pay-what-you-want on DriveThru RPG](https://www.drivethrurpg.com/product/89534/FU-The-Freeform-Universal-RPG)
   - [Hacks for customizing Freeform Universal](http://freeformuniversal.com/hacks/)
 - Fate (published by [Evil Hat](https://www.evilhat.com/))
   - [Fate Core at Evil Hat](https://www.evilhat.com/home/fate-core/)
   - [Fate Accelerated at Evil Hat](https://www.evilhat.com/home/fate-accelerated/)
   - [Old Fate website](https://www.faterpg.com/)
   - [Fate SRD (fan) website](https://fate-srd.com/)
 - [Dungeons and Dragons](https://dnd.wizards.com/) and [Pathfinder](https://paizo.com/pathfinder)
   - Look, you can find enough info about these on your own
   - The internet is full of articles about them
 - Open Game License (OGL) (... it's a weird license, but we'll talk about that some other time)
   - [Wikipedia article about the OGL](https://en.wikipedia.org/wiki/Open_Game_License)
   - [Open Game License 1.0 (Wayback machine copy, RTF file)](https://web.archive.org/web/20160302062643/http://www.wizards.com/d20/files/OGLv1.0a.rtf)
 - [Creative Commons Attribution 3.0 Unported license (CC BY 3.0)](https://creativecommons.org/licenses/by/3.0/)
 - [Dark Dungeons](https://www.chick.com/products/tract?stk=0046) (CW: 1980s religious satanic-panic propaganda... but for a fun time, find the 2014 live-action film somewhere)
 - [Rory's Story Cubes](https://www.storycubes.com/en/) (we called them "Rory's Story Dice" on the episode, oops)
 - [Donjon RPG tools](https://donjon.bin.sh/) (Don't have enough time to come up with your own materials?  You can use these as long as you don't mind leaning into a lot of auto-generated tropes.)
 - RPGs as therapy
   - [Please Don't Punch the GM: Adventures in Gaming Therapy](http://slangdesign.com/rppr/2016/02/panel-discussion/please-dont-punch-the-gm-adventures-in-gaming-therapy-at-pax-south-2016/) ([Role Playing Public Radio](http://slangdesign.com/rppr/) episode)
   - [Negotiating with the Dragon: Role-Playing Games as Group Therapy](https://wusfnews.wusf.usf.edu/post/negotiating-dragon-role-playing-games-group-therapy)
 - [Mary Robinete Kowal's Shades of Milk and Honey](http://maryrobinettekowal.com/novel/shades-of-milk-and-honey/) (first book in the [Glamourist Histories](http://maryrobinettekowal.com/faqs/about-shades-of-milk-and-honey/) books mentioned by Morgan as inspiring one of her game themes)
 - [Libre Lounge](https://librelounge.org/) (Podcast Chris used to co-host, still ongoing)
   - [Episode about history of Dungeons and Dragons, Pathfinder and the Open Gaming License](https://librelounge.org/episodes/episode-22-dungeons--dragons-free-culture-and-diversity-with-sean-hillman.html)

Still reading this?  Wow, okay, some bonus content...

We mentioned that Morgan did a class assignment for her intro-to-German
class about needing to make a German fairytale... here's the assignment,
as turned into the class:
[Hilda die Hexe](https://dustycloud.org/misc/the_witch_and_the_carriage-german.pdf)
(be nice enough to remember this is an intro-to-German class).
Additionally, Chris simultaneously wrote a slightly-more-elaborated-upon
version called
[The Witch and the Carriage](https://dustycloud.org/misc/the_witch_and_the_carriage.html).
The process of playing the game and then writing up both of these took
about an hour and a half so set your expectations accordingly.

Still want more?
Okay, not claiming this is a "great" story, but here's a kind of fun
writeup of a session called
[Santa's Little Uprising](https://dustycloud.org/misc/santas_little_uprising.html).
(Not very serious, could use more polish.)

Maybe we'll take up our own advice and more seriously publish some of
the stories we've constructed together sometime!

title: 46: Mark S. Miller on Distributed Objects, Part 1
date: 2022-05-31 22:50
tags: foss, objects, capabilities, ocaps, actors
slug: 46-mark-miller-on-distributed-objects-part-1
summary: Mark S. Miller walks us through the history of distributed object programming languages
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/046-mark-miller-on-distributed-objects-part-1.ogg" length:"27331307" duration:"00:41:27"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/046-mark-miller-on-distributed-objects-part-1.mp3" length:"39800822" duration:"00:41:27"
---
Calling all programming language nerds!  Distinguished computer
scientist Mark S. Miller (presently at [Agoric](https://agoric.com/))
joins us to tell us all about distributed object programming languages
and their history!  We talk about actors, a bit of Xanadu, and little
known but incredibly influential programming languages like Flat
Concurrent Prolog, Joule, and E!

Actually there's so much to talk about that this episode is just part
one!  There's more to come!

**Links:**

 - The [actor model](https://en.wikipedia.org/wiki/Actor_model#Fundamental_concepts)
   (the core of which is sometimes distinguished from modified variants
   by as being called "the classic actor model").  Long history;
   Tony Garnock-Jones' [History of Actors](https://eighty-twenty.org/2016/10/18/actors-hopl)
   is maybe the cleanest writeup

 - The
   [Agoric Open Systems papers](https://papers.agoric.com/papers/#aos)
   by Mark Miller and Eric Drexler are a good background into the 
   underlying motivations that got Mark into distributed objects

 - [markm-talks](https://www.youtube.com/playlist?list=PLKr-mvz8uvUgybLg53lgXSeLOp4BiwvB2)
   and
   [markm-more-talks](https://www.youtube.com/playlist?list=PLKr-mvz8uvUg70w0yKGfytaDqxiIBNo_L)
   which are *mostly* about object capability security topics

 - APConf keynote, [Architectures of Robust Openness](https://conf.tube/w/g87k3yKzYwpGhtohvQdC3k)
   by Mark S. Miller ([YouTube copy](https://www.youtube.com/watch?v=NAfjEnu6R2g))

 - [Mark diagraming a (certificate based) object capabilities flow at Rebooting Web of Trust 2017](https://share.tube/w/aaoHySAyiJPf3FUoxjAHbw) (when Mark and Christine first met!)

 - The history of Mark and company performing civil disobediance to
   make cryptography available to everyone is discussed in
   [When Encryption Was a Crime: The 1990s Battle for Free Speech in Software](https://www.youtube.com/watch?v=lv8OFSWZkGs),
   part of a [four part series](https://reason.com/video/2020/10/07/before-the-web-the-1980s-dream-of-a-free-and-borderless-virtual-world/)

 - [RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem))

 - [Xanadu](https://en.wikipedia.org/wiki/Project_Xanadu),
   [Ted Nelson](https://en.wikipedia.org/wiki/Ted_Nelson),
   and [Computer Lib/Dream Machines](https://en.wikipedia.org/wiki/Computer_Lib/Dream_Machines)

 - [Xerox PARC](https://en.wikipedia.org/wiki/PARC_(company)), which is where the Vulcan group happened (which is hard to find information on, sadly).
 
 - Mark mentions some of his colleagues who worked with him in the Vulcan group, including Dean Tribble (who worked on Joule, see more below) and [Danny Bobrow](https://en.wikipedia.org/wiki/Daniel_G._Bobrow) who is famous for his groundbreaking program [STUDENT](https://en.wikipedia.org/wiki/STUDENT_(computer_program)) ([Natural Language Input for a Computer Proglem Solving System](https://dspace.mit.edu/handle/1721.1/5922) is an incredible read, detailing a program (written in lisp!) which could read algebra "word problems" written in plain English and solve them... in 1964!).
 
 - Flat Concurrent Prolog... it's tough to find things about!  Presumably here's [the paper Mark mentioned that Dean lead on Flat Concurrent Prolog](https://dl.acm.org/doi/pdf/10.1145/323779.323739) from the Vulcan group which lead to Joule's channels.  [A bit more on (go figure) erights.org](http://www.erights.org/history/fcp.html)!

 - The [Joule manual](http://erights.org/history/joule/) is still a very interesting read, if you can find the time.  Talks about channels in depth.

 - Here's the [Communicating Sequential Processes book](http://www.usingcsp.com/cspbook.pdf) by [Tony Hoare](https://en.wikipedia.org/wiki/Tony_Hoare), quite a nerdy read!

 - On capabilities and actors... we'll get to this more in the next episode,
   but for now we'll leave the
   [Ode to the Granovetter Diagram](http://erights.org/elib/capability/ode/index.html)
   paper here (it's a truly amazing document!)


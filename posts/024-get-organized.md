title: 24: Get Organized!
date: 2021-03-12 14:20
tags: organization, org-mode, hipster pda
slug: 24-get-organized
summary: Morgan and Chris talk about self-organizational approaches which have worked for them, and which might work for you too!
enclosure: title:ogg url:"https://fossandcrafts.org/media/episodes/024-get-organized.ogg" length:"46829318" duration:"00:56:38"
enclosure: title:mp3 url:"https://fossandcrafts.org/media/episodes/024-get-organized.mp3" length:"54372251" duration:"00:56:38"
---
Morgan returns from handing in her dissertation!  Very topically,
Morgan and Chris talk about organizational systems which can help you
stay on track... even when you're working from home or trying to
finish your PhD during a global pandemic.

**Links:**

 - The [Hipster PDA](https://en.wikipedia.org/wiki/Hipster_pda),
   including the original
   [semi-satirical announcement post](http://www.43folders.com/2004/09/03/introducing-the-hipster-pda).

 - [Org Mode](https://orgmode.org/), the world's greatest
   organizational and outliner system (or so claims Chris), if you're
   an [Emacs](https://www.gnu.org/software/emacs/) user anyway
   (honestly, Org Mode is a great reason to pick up Emacs)

 - Locating purveyors of excessively priced office supplies left as
   an exercise for only a very particular kind of reader.

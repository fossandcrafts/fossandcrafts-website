compile_and_run: html serve

html:
	guix environment -l guix.scm -- haunt build

serve:
	guix environment -l guix.scm -- haunt serve --watch

upload:
	rsync --delete --recursive --verbose site/ neo.dustycloud.org:/srv/fossandcrafts/site/
